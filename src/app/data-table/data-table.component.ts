import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css']
})
export class DataTableComponent implements OnInit {

  carsData: any[];
  cols: any[];

  constructor( private dataService: DataService ) { }

  ngOnInit() {

    this.dataService.data.subscribe( dt => {
      this.carsData = [...dt];
    });
    this.dataService.getCars();

    this.cols = [
      { field: 'vin', header: 'Vin' },
      { field: 'brand', header: 'Brand' },
      { field: 'year', header: 'Year' },
      { field: 'color', header: 'Color' },
      { field: 'price', header: 'Price' },
      { field: 'type', header: 'Type' }
    ];
  }

  delData( vin ) {
    this.dataService.delData(vin);
  }

  setEditData( car ) {
    this.dataService.sendData(car);
    this.dataService.display();
  }

  display() {
    this.dataService.display();
  }

}
