export const CARS: any[] = [
    {'vin': 'a1653d4d', 'brand': 'VW',       'year': 2016, 'color': 'White',  'price': 10000, type: 'Jeep'},
    {'vin': 'ddeb9b10', 'brand': 'Mercedes', 'year': 2018, 'color': 'Green',  'price': 25000, type: 'SUV'},
    {'vin': 'd8ebe413', 'brand': 'Jaguar',   'year': 2016, 'color': 'Silver', 'price': 30000, type: 'MVP'},
    {'vin': 'aab227b7', 'brand': 'Audi',     'year': 2017, 'color': 'Black',  'price': 12000, type: 'MVP'},
    {'vin': '631f7412', 'brand': 'Volvo',    'year': 2015, 'color': 'Red',    'price': 15500, type: 'Sedan'},
    {'vin': '7d2d22b0', 'brand': 'VW',       'year': 2017, 'color': 'Maroon', 'price': 40000, type: 'Sedan'},
    {'vin': '50e900ca', 'brand': 'Fiat',     'year': 2019, 'color': 'Blue',   'price': 25000, type: 'SUV'}
  ];
