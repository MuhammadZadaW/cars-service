import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  lbl = 'Add';
  lbl2 = 'Reset';
  stat = null;

  display: boolean;

  carForm = new FormGroup({
    vin: new FormControl(''),
    brand: new FormControl(''),
    year: new FormControl(''),
    color: new FormControl(''),
    price: new FormControl(''),
    type: new FormControl('')
  });

  constructor( private dataService: DataService) { }

  ngOnInit() {
    this.dataService.dataSend.subscribe( dt => {
      this.carForm.patchValue(dt);
      this.stat = true;
      this.lblChange();
    });

    this.dataService.dataDisplay.subscribe( st => {
      this.display = st;
    });

  }

  save() {
    if (this.stat) {
      this.dataService.editData(this.carForm.value);
    } else {
      this.dataService.addData(this.carForm.value);
    }
    this.stat = null;
    this.lblChange();
    this.carForm.reset();
    this.display = false;
  }

  lblChange() {
    this.lbl = this.stat ? 'Save' : 'Add';
    this.lbl2 = this.stat ? 'Cancel' : 'Reset';
  }

  cancel() {
    this.carForm.reset();
    this.stat = null;
    this.display = false;
    this.lblChange();
  }

}
