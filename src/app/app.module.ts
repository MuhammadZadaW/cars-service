import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { InputTextModule } from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { DialogModule } from 'primeng/dialog';

import { AppComponent } from './app.component';
import { InputFormComponent } from './input-form/input-form.component';
import { DataTableComponent } from './data-table/data-table.component';
import { NullDirective, PatternDirective } from './input-form/null.directive';


@NgModule({
  declarations: [
    AppComponent,
    InputFormComponent,
    DataTableComponent,
    PatternDirective,
    NullDirective
  ],
  imports: [
    InputTextModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    RadioButtonModule,
    DialogModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
