import { Injectable } from '@angular/core';
import { CARS } from './data-table/car-list';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  cars: any[] = CARS;

  dataSource = new Subject<any[]>();
  data = this.dataSource.asObservable();

  sendDataSource = new Subject<any[]>();
  dataSend = this.sendDataSource.asObservable();

  displaySource = new Subject<boolean>();
  dataDisplay = this.displaySource.asObservable();

  constructor() {}

  getCars() {
    this.dataSource.next(this.cars);
  }

  delData( vin ) {
    const i = this.cars.indexOf(this.cars.find(cr => cr.vin === vin));
    this.cars.splice(i, 1);
    this.getCars();
  }

  addData( data ) {
    this.cars.push(data);
    this.getCars();
  }

  sendData( car ) {
    this.sendDataSource.next(car);
  }

  editData(car) {
    const i = this.cars.indexOf(this.cars.find(cr => cr.vin === car.vin));
    this.cars[i] = car;
    this.getCars();
  }

  display () {
    this.displaySource.next(true);
  }

}
